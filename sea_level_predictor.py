import pandas as pd
import matplotlib.pyplot as plt
from scipy.stats import linregress

def draw_plot():
    # Read data from file
    df = pd.read_csv("epa-sea-level.csv")
    df_2000 = df.loc[df["Year"] >= 2000]

    year_end = 2050

    # Create scatter plot
    plt.figure()

    plt.scatter(
        df["Year"],
        df["CSIRO Adjusted Sea Level"],
    )

    plt.scatter(
        df_2000["Year"],
        df_2000["CSIRO Adjusted Sea Level"],
    )

    # Create first line of best fit
    slope, intercept, _, _, _ = linregress(
        df["Year"],
        df["CSIRO Adjusted Sea Level"]
    )

    years_extended = range(df["Year"].min(), year_end + 1)
    sea_level_predicted = [slope * year + intercept for year in years_extended]

    plt.plot(years_extended, sea_level_predicted)

    sea_level_end = slope * year_end + intercept
    plt.scatter(year_end, sea_level_end)

    # Create second line of best fit
    slope, intercept, _, _, _ = linregress(
        df_2000["Year"],
        df_2000["CSIRO Adjusted Sea Level"]
    )

    years_extended = range(df_2000["Year"].min(), year_end + 1)
    sea_level_predicted = [slope * year + intercept for year in years_extended]

    plt.plot(years_extended, sea_level_predicted)

    sea_level_end = slope * year_end + intercept
    plt.scatter(year_end, sea_level_end)

    # Add labels and title
    plt.title("Rise in Sea Level")
    plt.xlabel("Year")
    plt.ylabel("Sea Level (inches)")

    # Save plot and return data for testing (DO NOT MODIFY)
    plt.savefig('sea_level_plot.png')
    return plt.gca()
